
public class SharedResourseMessageBox {
	private String message;
	
	public synchronized void setMessage(String message) {
		while (this.message == null) {
//		if (this.message == null) {
			this.message = message;
			notify();
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public synchronized String getMessage() {
		while (message == null) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String msg = message;
		message = null;
		notifyAll();
		return msg;
	}
}
