
public class ProducerSender extends Thread {
	SharedResourseMessageBox msgBox;
	
	public ProducerSender(SharedResourseMessageBox msgBox) {
		this.msgBox = msgBox;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			msgBox.setMessage("Hello " + i);
		}
	}
}