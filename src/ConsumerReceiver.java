
public class ConsumerReceiver extends Thread {
	SharedResourseMessageBox msgBox;
	
	public ConsumerReceiver(SharedResourseMessageBox msgBox) {
		this.msgBox = msgBox;
	}
	
	@Override
	public void run() {
		while (true) {
			System.out.println(msgBox.getMessage());
		}
	}
}
